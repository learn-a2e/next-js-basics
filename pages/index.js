//https://www.youtube.com/watch?v=MFuwkrseXVE&ab_channel=Academind
// import { useEffect, useState } from "react";
import { Fragment, useContext } from "react";
import Head from 'next/head';
import MeetupList from "../components/meetups/MeetupList";

const DUMMY_MEETUPS = [
  {
    id: "m1",
    title: "test title",
    image: "https://images.pexels.com/photos/733853/pexels-photo-733853.jpeg",
    address: "test address",
    description: "test description",
  },
  {
    id: "m2",
    title: "test title3",
    image: "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg",
    address: "test address3",
    description: "test description3",
  },
];

function HomePage(props) {
  /** useEffect and useState wont be needed since getStaticProps */
  // const [loadedMeetups,setLoadedMeetup]=useState([]);

  // useEffect(()=>{
  //   setLoadedMeetup(DUMMY_MEETUPS)
  // },[])

  return <Fragment><Head>
    <title>React title</title>
    <meta
    description="react description"
    content="test contents used"
    />
    </Head><MeetupList meetups={props.meetups} /></Fragment>;
}

//Reserved name
// only runs on server //client never sees //good for things which changes frequntly and incoming requests
export async function getServerSideProps(context) {
  const req = context.req;
  const res = context.res;

  return {
    props: {
      meetups: DUMMY_MEETUPS
    },
  };
}


//Reserved name //good for things which doesnt change frequntly
// export async function getStaticProps() {
//   //Any code write here execute in build process and not seen on client side
//   //always need to return an object

//   return {
//     props: {
//       meetups: DUMMY_MEETUPS
//     },

//     //incremental static generation //regenerated on the server and replaces old
//     //never older than 1 seconds
//     revalidate: 1
//   };
// }

export default HomePage;
