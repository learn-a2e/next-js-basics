//our-domain.com/news/details

import MeetupDetail from "../components/meetups/MeetupDetails";

function MeetupDetails(props) {
  return <MeetupDetail image={props.meetupData.image} title="First Meetup" />;
}

export async function getStaticPaths() {

  return {
    fallback:true,
    paths: [
      {
        params: {
          meetupId: "m1",
        },
        params: {
          meetupId: "m2",
        },
      },
    ],
  };
}

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId;

  return {
    props: {
      meetupData: {
        id: meetupId,
        image: "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg",
        title: "First Meetup",
      },
    },

    revalidate: 1,
  };
}

export default MeetupDetails;
