import classes from "./MeetupDetails.module.css";

function MeetupDetail(props) {
  return (
    <section className={classes.detail}>
      <img src={props.image} alt={props.title} />
      <h1>{props.title}</h1>
    </section>
  );
}

export default MeetupDetail;
